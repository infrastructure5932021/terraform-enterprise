######################################## calling GKE cluster module ####################################

module "gke" {
   source = "../module/gke-private"
   project_id = var.project_id
   region = var.region
   network_project_id = var.network_project_id
   name = var.gke_name
   kubernetes_version = var.kubernetes_version
   network = var.network
   subnetwork = var.subnetwork
   default_max_pods_per_node = var.default_max_pods_per_node
   ip_range_pods = var.ip_range_pods
   ip_range_services = var.ip_range_services
   enable_private_nodes = var.enable_private_nodes
   enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling
   master_ipv4_cidr_block = var.master_ipv4_cidr_block
   enable_private_endpoint = var.enable_private_endpoint
   enable_shielded_nodes =  var.enable_shielded_nodes
   network_policy = var.network_policy
   remove_default_node_pool = var.remove_default_node_pool
   machine_type       = var.machine_type 
   release_channel = var.release_channel
   #resource_labels = var.cluster_resource_labels
   create_service_account = var.create_service_account
   service_account = var.service_account
   impersonate_service_account  = var.impersonate_service_account
   node_pool_name               = var.node_pool_name
 
}




#######################################  Node Pool module ####################################

/*
module "node-pool-stateless" {
   source                           = "../module/node-pools"
   project_id                       = var.project_id
   network_project_id               = var.network_project_id
   region                           = var.region
   cluster                          = var.gke_name
   node_pool_name                   = var.node_pool_name
   node_pool_pod_range              = var.node_pool_pod_range
   node_pool_machine_type           = var.node_pool_machine_type
   node_pool_service_account        = var.node_pool_service_account
   node_pool_gke_version            = var.node_pool_gke_version
  
}

*/