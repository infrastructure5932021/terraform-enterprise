terraform {
  backend "remote" {
    
    organization = "prokopto-terraform"

    workspaces {
      name = "terraform-enterprise"
    }
  }
}
