module "gke-v2" {
   source = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
   version= "19.0.0" 
   project_id = var.project_id
   region = var.region
   network_project_id = var.network_project_id
   name = var.name
   kubernetes_version = var.kubernetes_version
   network = var.network
   subnetwork = var.subnetwork
   default_max_pods_per_node = var.default_max_pods_per_node
   ip_range_pods = var.ip_range_pods
   ip_range_services = var.ip_range_services
   enable_private_nodes = var.enable_private_nodes
   enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling
   master_ipv4_cidr_block = var.master_ipv4_cidr_block
   enable_private_endpoint = var.enable_private_endpoint
   enable_shielded_nodes =  var.enable_shielded_nodes
   network_policy = var.network_policy
   remove_default_node_pool = var.remove_default_node_pool
   create_service_account = var.create_service_account
   cluster_resource_labels   = { "mesh_id" : "proj-${var.project_id}" }
   release_channel = var.release_channel
   impersonate_service_account  = var.impersonate_service_account 
   
  # master_global_access_config = { enabled= "true"}
 
   master_authorized_networks = [
    
   {
      cidr_block   = "10.16.0.0/12"
      display_name = "CA VPN Range"
    },
    {
      cidr_block   = "192.168.45.0/24"
      display_name = "UK VPN Range"
    },


  ]
  
  node_pools = [
    
    {
      name               = var.node_pool_name
      initial_node_count = 1
      image_type         = "cos_containerd"
      machine_type       = var.machine_type
      min_count          = 1
      max_count          = 24
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      autoscaling        = true
      auto_upgrade       = true
      auto_repair        = true
      auto_upgrade       = true
      service_account    = var.service_account
      preemptible        = false
      enable_secure_boot = true
      max_pods_per_node  = 64
    },

   ]
   

   node_pools_taints = {
    all = []

    stateless = [
      {
        key    = "node-pool-type"
        value  = "stateless"
        effect = "NO_EXECUTE"
      },
    ]
  }
  
}
