##########################################################################################
#                                           GKE Variables
##########################################################################################

#Common Variables
variable "network_project_id" {
 default="prokopto-dev-host-project"
 description="shared VPC host project"
}

variable "project_id" {
  default="prokopto-dev-service-project2"
  description = "project ID of GCP or Service Project ID in case of shared VPC"
}

variable "region" {
  default="us-east1"
  description = "GCP Region"
}


#     GKE Variables

#For fetching kubernetes version
variable "location" {
  default="us-west1-a"
  description = "GCP Region"
}

variable "name" {
    default="prokopto-trio-cluster"
    description="GKE Cluster Name"
}

variable "enable_private_endpoint" {
  description = "(Beta) Whether the master's internal IP address is used as the cluster endpoint"
  default     = false
}

variable "enable_private_nodes" {
  description = "(Beta) Whether nodes have internal IP addresses only"
  default     = true
}

variable "enable_intranode_visibility" {
  description = "Whether Intra-node visibility is enabled for this cluster. This makes same node pod to pod traffic visible for VPC network."
  default     = true
}

variable "enable_vertical_pod_autoscaling" {
  description = "Vertical Pod Autoscaling automatically adjusts the resources of pods controlled by it"
  default     = true
}

variable "network_policy" {
  description = "Enable network policy addon"
  default     = true
}

variable "network_policy_provider" {
  type        = string
  description = "The network policy provider."
  default     = "CALICO"
}

variable "maintenance_start_time" {
  type        = string
  description = "Time window specified for daily or recurring maintenance operations in RFC3339 format"
  default     = "05:00"
}

variable "maintenance_exclusions" {
  type        = list(object({ name = string, start_time = string, end_time = string }))
  description = "List of maintenance exclusions. A cluster can have up to three"
  default     = []
}

variable "master_ipv4_cidr_block" {
  description = "(Beta) The IP range in CIDR notation to use for the hosted master network"
  default     = "10.0.0.0/28"
}

variable "default_max_pods_per_node" {
  description = "The maximum number of pods to schedule per node"
  default     = 64
}

variable "create_service_account" {
  description = "Defines if service account specified to run nodes should be created."
  default     = false
}

variable "service_account" {
  description = "The service account to run nodes as if not overridden in `node_pools`. The create_service_account variable default value (true) will cause a cluster-specific service account to be created."
  default     = "default"
}

variable "initial_node_count" {
  description = "The number of nodes to create in this cluster's default node pool."
  default     = 0
}

variable "release_channel" {
  description = "The release channel of this cluster. Accepted values are `UNSPECIFIED`, `RAPID`, `REGULAR` and `STABLE`. Defaults to `UNSPECIFIED`."
  default     = "REGULAR"
}

variable "kubernetes_version" {
    default = "1.21.6-gke.1500"
    description = "Kubernetes Version"
}

variable "remove_default_node_pool" {
  description = "Remove default node pool while setting up the cluster"
  default     = true
}

variable "machine_type" {
    default = "n1-standard-1"
    description = "machine type for GKE nodes"
}

variable "cluster_resource_labels" {
  description = "The GCE resource labels (a map of key/value pairs) to be applied to the cluster"
  default     = {
    "environment" = "development"
    "mesh_id" = "proj-465116717222"
  }
}

variable "network" {
    default="shared-vpc-net"
    description="Network Name"
}

variable "subnetwork" {
  default     = "service-project2-subnet"
  description = "Subnet Name"
}

variable "ip_range_pods" {
  default = "service-project2-pod-net-01"
  description = "cluster_secondary_range_name"
}

variable "ip_range_services" {
  default = "service-project2-service-net-01"
  description = "services_secondary_range_name"
}

variable "enable_shielded_nodes" {
  description = "Enable Shielded Nodes features on all nodes in this cluster"
  default     = true
}

variable "impersonate_service_account" {
  description = "An optional service account to impersonate for gcloud commands. If this service account is not specified, the module will use Application Default Credentials."
  default     = ""
}

variable "node_pool_name" {
   default = "default-pool"
}

/*variable "master_authorized_networks" {
# cidr_blocks = [
    {
      cidr_block   = "0.0.0.0/0"
      display_name = "all_network"
      }
  ]
}
*/
